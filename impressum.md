---
layout: page
title: Impressum & Datenschutz
permalink: /impressum/
script: impress
---

## Kontaktdaten / Contact information

<p id="address">Please enable JavaScript in your browser.</p>

<h3>Kontakt:</h3>

<table>
<tbody>
<tr>
<td>E-Mail:</td>
<td id="email">
    <em>Email address protected by JavaScript.<BR>
    Please enable JavaScript to contact me.</em>
</td>
</tr>
<tr>
<td>Telefon:</td>
<td id="phone">
    <em>Phone-Number protected by JavaScript.<BR>
    Please enable JavaScript to contact me.</em>
</td>
</tr>
</tbody>
</table>

Datenschutz
---

### Datenverarbeitung zur Übermittlung der Onlineinhalte

Zum Zweck der Übermittlung der von Ihnen aufgerufenen Webseite werden von Ihrem Browser typischerweise unter anderem folgende Informationen (im Rahmen von sogenannten HTTP-Requests) übersandt:

Ihre [IP-Adresse](https://de.wikipedia.org/wiki/IP-Adresse), die von Ihnen aufgerufene [URL](https://de.wikipedia.org/wiki/Uniform_Resource_Locator), Informationen zu dem von Ihnen verwendeten Browser und ggf. Betriebssystem, wie deren Name und Version ([User-Agent](https://de.wikipedia.org/wiki/User_Agent#Aufbau_des_HTTP-User-Agent-String)), sowie – unter Umständen – die Seite, von der aus Sie auf dieser Webpräsenz gelangt sind ([Referrer-Information](https://de.wikipedia.org/wiki/Referrer)) und natürlich auch den Zeitpunkt der Anfrage. Da das senden von Referrer-Informationen und User-Agent nicht Serverseitig unterbunden werden kann, und somit automatisch Teil der Kommunikation im Rahem von HTTP Anfragen werden, sind diese ebenso wie die IP-Address und natürlich die angefrage URL gemäß [Art. 6.1b DSGVO](https://dsgvo-gesetz.de/art-6-dsgvo/) zu verarbeiten.

Diese Daten werden kurzfristig zur Beantwortung ihrer Anfrage im RAM des Servers vorgehalten. Weiterhin werden IP-Adresse und URL können zudem für weitere sieben Tage in Protokolldateien gespeichert werden, um mögliche Störungen der Seite analysieren zu können. Sofern solche Störungen auftauchen, kann die Speicherung im Einzelfall auch länger andauern. Sie werden spätestens dann gelöscht, wenn sie zur Ermittlung der Ursachen der Störung nicht mehr erkennbar beitragen können. ([Art. 6.1f DSGVO](https://dsgvo-gesetz.de/art-6-dsgvo/))


### Cookies

#### Blog (openalchem.ist)

Es werden keine Cookies verwendet.

---

Privacy
---

### Data usage for content transmission

To receive the content of this web page your browser sends the following information during an so-called HTTP request:

Your [IP-Address](https://en.wikipedia.org/wiki/IP_address), your requested [URL](https://en.wikipedia.org/wiki/URL), information about the browser and sometimes also operating system you use ([User-Agent](https://en.wikipedia.org/wiki/User_agent#Format_for_human-operated_web_browsers)) as well as the page which was used to visit this page ([referrer-information](https://en.wikipedia.org/wiki/HTTP_referer)) and obviously the time at which you are question the page. As there is no way to prevent your browser from sending the user agent string or referrer information and this way they become part of the communication, as your IP address and the requested URL does. Those information are required and used to answer your request (Art. 6.1b [GDPR](https://gdpr-info.eu/art-6-gdpr/)).

This data is stored in the server's [RAM](https://en.wikipedia.org/wiki/Random-access_memory) to answer your request. Your IP address and the requested URL can be stored up to 7 days due to analyze possible problems with the webpage. Given that a problem appears there are cases, where it's possible that this information is stored longer than 7 days to debug the problem in depth. The data is deleted as soon as it's confirmed that they can no further help with diagnosing the problem or the problem is solved. (Art. 6.1f [GDPR](https://gdpr-info.eu/art-6-gdpr/))

### Cookies

#### Blog (openalchem.ist)

This web page doesn't use any cookies.

---

**[Eine Änderungshistorie dieses Dokuments finden Sie auf GitLab](https://gitlab.com/w4tsn/openalchemist-blog/commits/main/impressum.md)**
