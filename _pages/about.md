---
layout: page
title: "The Open Alchemist"
author: "w4tsn"
permalink: /about/
---

Hello there!

My name is Alex. I am posting from the sweet city of Darmstadt in the Rhein-Main-Area. As co-founder and CTO of [othermo GmbH](https://www.othermo.de), operator of a small ActivityPub-Community on [darmstadt.social](https://darmstadt.social) and maintainer of this and that in the FOSS ecosystem, on these pages I mostly write about technical stuff, experiences and my professional journey.

For discussions I invite you to contact me publicly on Mastodon via [@w4tsn@darmstadt.social](https://darmstadt.social/@w4tsn).

If you want to drop me a PGP encrypted message [use my public key]({{ '/w4tsn.pgp.pub' | prepend: site.baseurl }}).

I'm also available on matrix as @w4tsn:darmstadt.social. If you don't have a matrix account yet, feel free to join matrix.darmstadt.social via [talk.darmstadt.social](https://talk.darmstadt.social)

If you just want to chill out on a cup of coffee drop me a Jitsi link from [meet.darmstadt.social](https://meet.darmstadt.social) with whatever channel you like.

500 Chars About Me
---

I'm an intuitive and turbulent type. Also an architect, being it social behaviour - personal or structural - morals or software systems. My humor is quite broken. I like logic, riddles and to play with words. A world without music or art would be devastating. I love board games, writing and movies. A shared flat is where I live, love and linger. I like niche topics like degrowth, minimalism and anime. I tend to get really passionate and opinionated about topics of moral and social justice.

"Open Alchemist"
---

The name is a tribute to the mystical, fantasy idea of an alchemist. OK, you got me. It's a reference to the Alchemists from the Anime "Fullmetal Alchemist Brotherhood". A base principal of my work is decomposition, understanding and reconstruction. 'Open' because I love the FOSS ideology and opposed to traditional alchemists I like to lay my work out openly before me.

Support
---

If you find what I'm doing valuable, don't hesitate to tell me via any known channel. Your feedback is much appreciated.
