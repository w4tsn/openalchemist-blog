---
layout: post
title:  "Docker was Hungry!"
author: "w4tsn"
date:   2017-09-06
excerpt: "It just ate all my disk space."
tag:
- container
---

Two days ago <b>Docker</b> just decided it was really hungry and began to eat up a lot of disk space. Finally I noticed because some container complained about having no disk space left to run a shell command. 0 bytes to be specific.

Hmmm.

Shortly after other programs complained about disk space and some of them stopped working. together with [Sheogorath](https://shivering-isles.com) we investigated the problem and he found out that the storage driver `aufs` I was using could cause problems - as [mentioned on stackoverflow](https://stackoverflow.com/a/27894675).

I'm running arch-based Manjaro linux so I looked up the [arch wiki pages on the topic](https://wiki.archlinux.org/index.php/Docker) and configured my docker installation to use `overlay2` instead. Was it really that easy? Of course not.

Actually I first issued the command `$ df -h` to get something like this:

```
Filesystem      Size  Used Avail Use% Mounted on
dev             5,9G     0  5,9G   0% /dev
run             5,9G  1,7M  5,9G   1% /run
/dev/sda2       221G  221G  0,0G 100% /
tmpfs           5,9G  153M  5,7G   3% /dev/shm
tmpfs           5,9G     0  5,9G   0% /sys/fs/cgroup
tmpfs           5,9G   44M  5,8G   1% /tmp
/dev/sda1       300M  256K  300M   1% /boot/efi
tmpfs           1,2G   60K  1,2G   1% /run/user/1000
```

Then I typed `$ du -sbh /var/lib/docker/aufs` which gave me `114 GB /var/lib/docker/aufs/` as a result and there it was - the hungry space eater!

I removed docker and remaining files, installed it again and configured it to use `overlay2`.