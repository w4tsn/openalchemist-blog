---
layout: post
title:  "Dying through Dependency Management"
author: "w4tsn"
date:   2017-09-08
excerpt: "When a simple headless build is about to kill you"
tag:
- rant
- progesture
---

**Oh. My. God.** I was close. Really close. So close to death because I tried to run a headless build of an Eclipse RCP application. It started harmless, as always. You know that feeling. You have that running code base that builds just fine, which started from a simple idea until it grew into something big. You test something, it works on your system so you push... and boom. It crashes on the other machines and hell breaks loose. 

Then, as it was the most natural thing on the world, suddenly it comes to your mind. Continuous Integration! (OK, you could instead just build stuff that works and everything would be alright - but let's be honest)

So I started to convert my [Eclipse RCP](https://wiki.eclipse.org/Rich_Client_Platform) [plugin](http://www.vogella.com/tutorials/EclipsePlugin/article.html) and [feature](http://www.vogella.com/tutorials/EclipsePlugin/article.html#exercise-create-a-feature-for-your-plug-in) projects into a headless build ready [maven/tycho](https://eclipse.org/tycho/) environment. In principle that was really easy with tutorials from [Lars Vogel](http://www.vogella.com/tutorials/EclipseTycho/article.html) and fellow sufferers on [stackoverflow](https://stackoverflow.com/). I created the pom.xml files as described in the tutorials and restructured my project just to be safe. 

After some tweaking to the configs I was presented with the following error:

```
[ERROR] Cannot resolve project dependencies:
[ERROR]   Software being installed: org.myplugin.someservice 1.0.0
[ERROR]   Missing requirement: org.myplugin.someservice 1.0.0 requires 'bundle org.eclipse.e4.core.di 0.0.0' but it could not be found
```

Well. Wow. That got me somehow - indignant. After all it was working within reproducible development environment. I made sure that all dependencies were properly defined in the projects `Neon.target` file. I double-checked all configurations, which lead to some serious config refactor. With no success. At least the configs were well structured and redundancy free.

I started to search the web for similar problems, but you know what? Like anytime I try to learn something about Eclipse RCP and Plug-in development or when trying to troubleshoot problems in this domain I end up really disappointed asking myself "Is anybody out there actually using this technology?". Eclipse as RCP is really powerful and provides a great amount of freedom. In my experience however it is a pain in the a** to learn how it works. Except of maybe three books, there is no really up-to-date in-depth coverage of some of the more advanced use-cases. I'm really happy that if you ask stackoverflow and there actually is an answer, it is helpful.

Finally I [asked stackoverflow](https://stackoverflow.com/questions/46102903/eclipse-rcp-maven-tycho-missing-requirement-bundle-org-eclipse-e4-core-di/46120605#46120605). In the end I found a solution to my problem, though it might not be how the developers of Tycho intended. You can read that up on stackoverflow.

After about 8 hours of work (yeah, I couldn't accept a defeat :/) it felt like a much greater victory as it might be in reality. Never mind. I survived and now I can rest in peace at last.