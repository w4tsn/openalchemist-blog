---
layout: post
title:  "Maintaining A Fedora IoT Remix And It's Actually Fun!"
author: "w4tsn"
date:   2021-02-11
excerpt: "Let me introduce you to the amazing topic of building and maintaining your own Fedora IoT OS/Firmware Remix."
tag:
- fedora
- othermo
- work
---

Let me introduce you to the amazing topic of building and maintaining your own Fedora IoT OS/Firmware Remix.

I want to use [Fedora IoT](https://docs.fedoraproject.org/en-US/iot/) on Raspberry Pi (and a multitude of other devices), but not without my own customizations, scripts and all that flavour. So I set out on an adventure to build and maintain a custom variant of Fedora IoT, a so-called Remix, because it's a great system and surprisingly fitted to be customized.

I'm In Need For Some Remix
---

I'm currently in need for a solid firmware operating system based on linux with some very specific requirements - both for my work and all the Raspberry Pis in my possession. Based on the work of a friend and former colleague of mine[^1] and his research as part of his bachelor thesis I came to a quite opinionated solution based on Fedora IoT.

Fedora IoT has some nifty features:

- automated updates and rollbacks in case anything goes wrong
- small update footprint through deltas
- modern, state-of-the-art technologies
- hybrid read-only/writable filesystem with (almost) only configuration paths persistent
- designed with containerization in mind
- low general footprint
- runs on a multitude of hardware devices
- is standard and close to upstream
- huge plus: it's familiar to me as a Fedora user

With tools such as RPM-OSTree, Gitlab, Gitlab CI, Gitlab-Runner, EC2, Shell-Scripting, Containers, NGINX and so on we'll look into how to build and maintain a Remix of Fedora IoT. But let's not get ahead of ourselves.

The Journey Ahead
---

What I want as a start is placing my own shell-scripts, systemd units, configurations and [RPM](https://docs.fedoraproject.org/en-US/fedora/rawhide/system-administrators-guide/RPM/)s onto multiple devices, let's say all my Raspberry Pis. So coming from stock Fedora IoT a base requirement is to ship my own [RPM-OSTree](https://coreos.github.io/rpm-ostree/) remote since that's the source this system is receiving it's updates from. OSTree allows to specify different remotes and switch freely between them and their respective branches and/or commits. So after we've setup a repo we can use the [`rpm-ostree` command](https://www.mankier.com/1/rpm-ostree) with e.g. `rpm-ostree rebase myremote:mystuff/stable/aarch64` on the Fedora IoT host to switch over to our own repo, branch and architecture.

I figure the first thing and most interesting one to look at is building custom [OSTree](https://ostreedev.github.io/ostree/) commits on top of stock Fedora IoT to fill our remote with some life. The sources for those custom commits come from a git repository which is not to be confused with an [OSTree repository](https://ostreedev.github.io/ostree/repo/). After that we can take a look at how to serve this OSTree repository we just built, either openly or behind some authentication mechanism like mTLS. With the foundation setup and the basics clear we can move forward to bring some automagic into play and use CI to build new stuff for new source commits. While automating the build process is quite nice on it's own, this should definitely happen on our existing OSTree repository and also update it with the result instead of creating a new repo every time. So we'll spent some time on bringing the remote repo into the CI.

While all of the things above can be done in a multitude of ways I'll give you my approach as one of many possibilities and hope you'll find your own way together with the utter excitement of this topic. *~amazing*

References
---

[^1]: Checkout his blog: [www.shivering-isles.com](https://www.shivering-isles.com)
