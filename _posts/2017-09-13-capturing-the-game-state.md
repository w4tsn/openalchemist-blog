---
layout: post
title:  "Records Of The Game State"
author: "w4tsn"
date:   2017-09-13
excerpt: "How Odysseus will approach the recording of the game state"
tag:
- odysseus
- machinelearning
---

The Odysseus implementation for a machine learning driven strategy generation is composed of the following key components:

- information **capturing** and **representation**
- An interface to plug-in *intelligence*
  - for **processing data**
  - to let it **act**

First things first let us have a look at ways to retrieve data from the game for now and discuss how to represent it from an engineering perspective.

## From an Engineering Perspective

Lets start with a first glimpse at the required components from an engineering perspective. We have several classes and objects in order to represent the data for a machine learning algorithm in a useful way. It should also fulfil the requirement of being a well-defined interface to prevent later confusion and errors between trained models and the corresponding interface revision.

* `Record`: Is the central data structure. A sample within a match including information from the viewpoint of the bot.
* `MatchRecord`: Is a set of `Record` objects, capturing a whole match and corresponding meta data.
* `MatchRecordWriter`: Can write `MatchRecord` in various formats to disk.
* `MatchRecordReader`: Can read `MatchRecord` in various formats from disk.
* `MatchRecorder`: Is a management object (singleton) to create `Record` and `MatchRecord` within the update loop. Provides also several output, load and save capabilities.

## In-depth View

### Record

The `Record` is a constant object, which can not be altered. It has two constructors. One captures the current state of a match, the other constructs the object out of another source e.g. a file.

At its construction it captures the following information:

- frame
- Number and type of units
- Number of bases
- Number and type of buildings
- Number and type of resources
- researched tech
- upgrades
- Number and type of buildings with free production capabilities
- Number of idle workers

### `MatchRecord` Class

A `MatchRecord` is a dynamic structure, which receives `Record` objects until it is closed. After that it is saved and cannot be altered again, since it captures a finished game.

It captures the following information:

- frame step capture size
- interface revision for compatibility
- vector of `Record` objects
- self race for selection of model
- enemy race (also Unknown)
- resource statistics
- win/loose
- custom statistics

### `MatchRecorder` Class

`MatchRecorder` is a management class creating a `MatchRecord` for a game and filling it with `Record` objects at defined time steps.

It is also controlling different output streams and formats of how and when the data is loaded or saved.

It holds information about the distance between records and the revision number of the machine learning interface. More on why a revision number is needed in a future post about the machine learning interface.

## Difference to Steamhammer

As mentioned before is Jay Scott also working on an opponent model in Steamhammer. While one difference is of course that this approach is aimed at more universal models, the difference in the implementation is the use of a single `Record` class containing the whole state instead of capturing each player as separate record, since all information belongs together and is always taken from the perspective of the playing bot.

Since I'm aim at learning a single model per race I'm not directly interested in the exact match up but only in the own race. I will also try the potential of a single model for all races, while I've no clue how the learning performance will suffer with that much distraction potential. I'll talk more about what I mean by distraction in the next posts.