---
layout: post
title:  "First Gaze At The Neural Net"
author: "w4tsn"
date:   2017-09-28
excerpt: "Let's talk about serious stuff!"
tag:
- odysseus
- machinelearning
---

After the infrastructure is on the go now I can spent time on writing my thesis about it! Besides I'll have a more in-depth look at the trained neural net and it's properties. The first deep net will be hooked in by the `predictor` and calculate a win/loose-prediction. Let's have a look at how that works and how I'm going to research this further.

## Details about the Neural Net

Right now the model is encoded in a 5 layer deep neural net with around 380 features as input and a win/loose probability as output. For now let's talk about the training process first. Of course the network will soon receive a more in-depth post with a detailed analysis about it's precision/recall, learning curves, structure and other properties.

The current precision when trained on all training examples is around 70 %. When including the final scores into all examples the precision rises to around 93 %, which is to be expected since they give a really good estimate about the winning probability - at least if the opponents are not to close in terms of skill level. Another interesting fact occurs if we strip away training examples from early on in a game. The model becomes more and more precise, indicating good generalization. For examples `> 625 seconds` we receive `precision ~ 80 %` and for `> 833 seconds` `precision ~ 90 %` while a game lasts around 625 seconds in average.

Right now the model is trained only from the perspective of Odysseus itself. To further expand the quality of this feature there are several interesting things to have a look at regarding especially the properties of the neural net:

* What influence has the perspective of the capturing?
* Is it useful to include the map as a feature?
* What changes when including training examples from self-play

To test the influence of perspective I'll integrate the capturing feature into other bots where I can get a grasp on the source code. This is fairly easy due to the loosely coupled architecture. The new training examples will be included and the results compared to those without. If the model is already generalized it should not result in a big impact.

Because the map should have at least some influence on the effectiveness of units and the bots play style there should be an improvement in the prediction when taking that piece of information into consideration.

When playing against itself it will not only produce twice as much data it will also learn about it's own flaws to some extend. Here I'm actually really curious considering current results of DeepMind and OpenAI on self-play methods about the effect it has on the results of the training.