---
layout: post
title:  "Introducing the Map feature"
author: "w4tsn"
date:   2017-09-25
excerpt: "Strategy intrinsically depends on the map you play on."
tag:
- odysseus
- machinelearning
---

The last four days I fixed several more bugs in the feature capturing process and let the bot play and capture about 1000 games which results in approximately 60.000 training examples. This should be sufficient for a first analysis and application to begin with.

The first thing I'll do with this data is learning a prediction model for the outcome of games. This will give the bot a good start in understanding it's own current position in a match. Based on this self-evaluation it will be easier to make decisions. Current accuracy is around .93 %, which seems to be precise enough while remaining fairly general.

While capturing the data I noticed and remembered the importance of the influence the map has on the performance of the bot and it's utilized algorithms or libraries. This was not captured in the first feature vector. I setup a map to assign IDs for all known maps to correlate it with the rest of the strategic information. If a map is unknown it will receive a value of 0 at the moment, which corresponds to no activation of any input-neuron and therefore no influence in the model. Later it is planned to generate an ID map to automatically assign and save IDs for new maps and save it as JSON file.

Next up will be the implementation of this model, so the bot is actually able to use it in-game.
